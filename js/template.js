template = {

  /**
   * load a file referenced by url into a tag
   *
   * url is a full qualified url that an be used for http ajax requests
   * tag_id is the id of the element to load the chunk to
   */
  load: function(url, tag_id) {
    $.get(url, function(data) {
      $("#" + tag_id).html(data);
    });
  },

  /**
   * load the content of url into the tag with the id main
   */
  reload_main: function(url) {
    $.get(url, function(data) {
      $("#main").html(data);
      // switch off navigation on mobile
      $('.navbar-collapse').collapse('hide');
    });
  },

  /**
   * load a todo message instead of the content
   */  
  reload_main_todo: function(url) {
    $("#main").html("Leider wird dieser Bereich gerade überarbeitet. Bitte besuche diese Seite später wieder.");
    // switch off navigation on mobile
    $('.navbar-collapse').collapse('hide');
  },
  
  /**
   * reset the title in the navbar
   */
  reset_title: function(name) {
    $("#title").html(name);
    // hack: ensure right banner background color
    darkmode = $("body").attr("darkmode");
    if (darkmode == "true") {
      $(".banner").css('background-color', '#282828');
    }
  },
   
  /**
   * switch the dark mode
   */
  switch_mode: function() {
    darkmode = $("body").attr("darkmode");
    if (darkmode == "true") {
      $('body').css('background-color', '#fff');
      $('#menubar').removeClass('navbar-light').addClass('navbar-dark');
      $('#menubar').removeClass('bg-light').addClass('bg-dark');
      $('.dropdown-item').removeClass('lightmode-textcolor').addClass('darkmode-textcolor');
      $('.nav-link').removeClass('lightmode-textcolor').addClass('darkmode-textcolor');
      $(".banner").css('background-color', '#fafafa');
      $("#darkmodebutton").removeClass('darkmodebutton');
      darkmode = false;
    } else {
      $('body').css('background-color', '#111');
      $('#menubar').removeClass('navbar-dark').addClass('navbar-light');
      $('#menubar').removeClass('bg-dark').addClass('bg-light');
      $('.dropdown-item').removeClass('darkmode-textcolor').addClass('lightmode-textcolor');
      $('.nav-link').removeClass('darkmode-textcolor').addClass('lightmode-textcolor');
      $(".banner").css('background-color', '#282828');
      $("#darkmodebutton").addClass('darkmodebutton');
      darkmode = true;
    }
    $("body").attr("darkmode", darkmode);
    // switch off navigation on mobile
    $('.navbar-collapse').collapse('hide');
  }
}
